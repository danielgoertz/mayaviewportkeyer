//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+

// Description: 
//   Demonstrates how to create your own custom image plane based on
//   Maya's internal image plane classes. This allows API users to
//   override the default Maya image plane behavior. This class works
//   like typical API nodes in that it can have a compute method and
//   can contain static attributes added by the API user.  This
//   example class overrides the default image plane behavior and
//   allows users to add transparency to an image plane using the
//   transparency attribute on the node. Note, this code also 
//   illustrates how to use MImage to control the floating point
//   depth buffer.  When useDepthMap is set to true, depth is added
//   is added to the image such that half of the iamge is at the near 
//   clip plane and the remaining half is at the far clip plane. 
//
//   Note, once the image plane node has been created it you must
//   attached it to the camera shape that is displaying the node.
//   Image planes are attached to cameras via the imagePlane message
//   attribute on the camera shape.  To attach this example image
//   plane you should connect the image plane's message attribute to
//   the cameraShapes imagePlane attribute.  Note, the imagePlane
//   attribute is a multi attribute and can hold references to
//   multiple image planes.
//
//   This example works only with renderers that use node evaluation
//   as a part of the rendering process, e.g. Maya Software. It does 
//   not work with renderes that rely on a scene translation mechanism,
//   e.g. mental ray.
// 
//   For example, 
//     string $imageP = `createNode mayaImagePlane`
//	   connectAttr -f ($imageP + ".message") "perspShape.imagePlane[0]"
//    

#include <maya/MPxImagePlane.h>
#include <maya/MFnPlugin.h>
#include <maya/MImage.h>
#include <maya/MString.h>
#include <maya/MFnNumericAttribute.h> 
#include <maya/MFnNumericData.h> 
#include <maya/MDataHandle.h> 
#include <maya/MPlug.h> 

#include <iostream> // for standard I/O
#include <string>   // for strings
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion

#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc/imgproc.hpp>  // Gaussian Blur
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O

using namespace std;
using namespace cv;

class mayaImagePlane : public MPxImagePlane
{
public:
						mayaImagePlane();
	virtual MStatus		loadImageMap( const MString &fileName, int frame, MImage &image );

	void				refreshImage();

	virtual bool		getInternalValueInContext( 
		const MPlug&, MDataHandle&,  MDGContext&);

    virtual bool		setInternalValueInContext( 
		const MPlug&, const MDataHandle&, MDGContext&);

	static  void*		creator();
	static  MStatus		initialize();

	static	MTypeId		id;				// The IFF type id
	static  MObject		aTransparency; 
	static  MObject		aHue;
	static  MObject		aThreshold;

private: 
	double				fTransparency;
	double				fHue;
	double				fThreshold;
};

MObject mayaImagePlane::aTransparency; 
MObject mayaImagePlane::aHue; 
MObject mayaImagePlane::aThreshold; 

mayaImagePlane::mayaImagePlane() : 
	fTransparency( 0.0 )
{
}


bool		
mayaImagePlane::getInternalValueInContext( 
	const MPlug &plug, MDataHandle &handle,  MDGContext& context )
{
	if ( plug == aTransparency ) { 
		handle.set( fTransparency ); 
		return true; 
	}
		
	return MPxImagePlane::getInternalValueInContext( plug, handle, context ); 
}

bool		
mayaImagePlane::setInternalValueInContext( 
	const MPlug &plug, const MDataHandle &handle, MDGContext &context)
{
	if ( plug == aTransparency ) { 
		fTransparency = handle.asDouble();
		setImageDirty();
		return true; 
	}
	
	return MPxImagePlane::setInternalValueInContext( plug, handle, context );
}

MStatus	
mayaImagePlane::loadImageMap( 
	const MString &fileName, int frame, MImage &image )
{
	image.readFromFile(fileName);
	
	VideoCapture cap(0);

    if(!cap.isOpened()){  // check if we succeeded
        cout << "DEBUG: Device is offline" << endl;
		return MStatus::kFailure;
	}

	unsigned int width;
	unsigned int height;
	double frate;

    for(;;)
    {
		width = (int) cap.get(CV_CAP_PROP_FRAME_WIDTH);
		height = (int) cap.get(CV_CAP_PROP_FRAME_HEIGHT);
		frate = cap.get(CV_CAP_PROP_FPS);
		cout << "Video Dimensions: " << width << " x " << height << " @ " << int(frate) << " FPS" << endl;

		Mat frame;
		cap >> frame; // get a new frame from camera
		if (frame.empty()) break;

		uchar* camData = new uchar[frame.total()*4];
		Mat continuousRGBA(frame.size(), CV_8UC4, camData);
		cvtColor(frame, continuousRGBA, CV_BGR2RGBA, 4);

		unsigned int size = width * height;
		unsigned char *buffer = new unsigned char[size * 4];

		image.resize(width, height, true);
		image.setRGBA(true);
		unsigned char *pixels = image.pixels(); 

		bool readframe = cap.read(frame);

		for (unsigned int y = 0; y < height; y++)
		{
			unsigned char* pPixel = camData + (y * width * 4);
			for (unsigned int x = 0; x < width; x++)
			{
				Vec3b pixelcolor = frame.at<Vec3b>(Point(x, y));
				unsigned char alpha=1;
				bool checker = (((x >> 5) & 1) ^ ((y >> 5) & 1)) != 0;
				pixelcolor[0] = 0;
				pixelcolor[1] = 0;
				pixelcolor[2] = checker ? 255 : 0;
				
				*pPixel++ = pixelcolor[0];
				*pPixel++ = pixelcolor[1];
				*pPixel++ = pixelcolor[2];
				*pPixel++ = (unsigned char)((1.0-fTransparency)*255*alpha);
			}
		}
		image.setPixels( camData, width, height );
		cap.release();
		delete [] camData;
	}

	return MStatus::kSuccess;
}

void
mayaImagePlane::refreshImage()
{
	cout << "BANG" << endl;
	return;
}

MTypeId mayaImagePlane::id( 0x1A19 );

void*
mayaImagePlane::creator()
{
	return new mayaImagePlane;
}

MStatus
mayaImagePlane::initialize()
{
	MFnNumericAttribute nAttr;	
	aTransparency = nAttr.create( "transparency", "tp", 
								  MFnNumericData::kDouble, 0 );
	nAttr.setStorable(true); 
	nAttr.setInternal(true); 
	nAttr.setMin(0.0); 
	nAttr.setMax(1.0); 
	nAttr.setDefault(0.0);
	nAttr.setKeyable(true); 
	addAttribute( aTransparency ); 

	MFnNumericAttribute nAttrHue; 	
	aHue = nAttrHue.create( "hue", "hu", 
								  MFnNumericData::kDouble, 0 );
	nAttrHue.setStorable(true); 
	nAttrHue.setInternal(true); 
	nAttrHue.setMin(0.0); 
	nAttrHue.setMax(255.0); 
	nAttrHue.setDefault(180.0);
	nAttrHue.setKeyable(true); 
	addAttribute( aHue );

	MFnNumericAttribute nAttrThresh; 	
	aThreshold = nAttrThresh.create( "threshold", "thr", 
								  MFnNumericData::kDouble, 0 );
	nAttrThresh.setStorable(true); 
	nAttrThresh.setInternal(true); 
	nAttrThresh.setMin(0.0); 
	nAttrThresh.setMax(1.0); 
	nAttrThresh.setDefault(0.01);
	nAttrThresh.setKeyable(true); 
	addAttribute( aThreshold );

	return MStatus::kSuccess;
}

// These methods load and unload the plugin, registerNode registers the
// new node type with maya
//
MStatus initializePlugin( MObject obj )
{ 
	MStatus   status;
	MFnPlugin plugin( obj, "mayagroup", "1.0", "Any");

	status = plugin.registerNode( "mayaImagePlane", mayaImagePlane::id, 
								  mayaImagePlane::creator,
								  mayaImagePlane::initialize, 
								  MPxNode::kImagePlaneNode );
	if (!status) {
		status.perror("registerNode");
		return( status );
	}

	return( status );
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   status;
	MFnPlugin plugin( obj );

	status = plugin.deregisterNode( mayaImagePlane::id );
	if (!status) {
		status.perror("deregisterNode");
		return( status );
	}

	return( status );
}